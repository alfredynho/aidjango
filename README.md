## Web Site Django

**Project Django BlogApp ** Web Site con Django
### Tecnologías
    Django
    Virtualenv 
    Python
    Linux
    -- Postgresql

# DJBlog
![alt text](https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png)


# virtualenv

  - `virtualenv -p python3 env`
  - `source env/bin/activate`
  - `pip install django`

# Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE blogapp"`
  - `psql -c "DROP USER bloguser"`
  - `psql -c "CREATE USER bloguser WITH ENCRYPTED PASSWORD '122333'"`
  - `psql -c "CREATE DATABASE blogapp WITH OWNER bloguser"`
  

# Base de Datos Sqlite3
  El proyecto se encuentra por defecto configurado para ser ejecutado con sqlite3
  
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

[@alfredynho](alfredynho.cg@gmail.com).
[Telegram](@alfredynho).